#! /usr/bin/env bash

set -e

REPO_URL=https://orion.agriculture/artifactory/Map

M2_REPO=/home/fred/.m2/repository; # TODO FDA 2019/03 A récupérer dans la balise "localRepository" du fichier "~/.m2/settings.xml".

wget="wget --user=$REPO_LOGIN --password=$REPO_PASSWORD  --no-check-certificate"

pgm=$0
#echo "pgm = '$pgm'"

if [[ ! -z $1 ]]; then
    dir=$1
    shift
else
    dir=${M2_REPO}
fi

set +e
#set -x
lastUpdateds=$( \ls ${dir}/*.lastUpdated 2> /dev/null )
set -e
#echo "lastUpdateds = '$lastUpdateds'"
for lu in ${lastUpdateds}; do
    #           /home/fred/.m2/repository/fr/gouv/agriculture/orion/jsf/theme/orion-less-web/5.4/orion-less-web-5.4.pom.lastUpdated
    grp_id=$(   echo ${lu} | sed -e "s/^\/home\/fred\/\.m2\/repository\/\(.*\)\/\([^\/]*\)\/\([^\/]*\)\/\([^\/]*\)\.\([^\/\.]*\)\.lastUpdated$/\1/" )
    art_id=$(   echo ${lu} | sed -e "s/^\/home\/fred\/\.m2\/repository\/\(.*\)\/\([^\/]*\)\/\([^\/]*\)\/\([^\/]*\)\.\([^\/\.]*\)\.lastUpdated$/\2/" )
    art_vers=$( echo ${lu} | sed -e "s/^\/home\/fred\/\.m2\/repository\/\(.*\)\/\([^\/]*\)\/\([^\/]*\)\/\([^\/]*\)\.\([^\/\.]*\)\.lastUpdated$/\3/" )
    #art_fqn=$(  echo ${a} | sed -e "s/^\/home\/fred\/\.m2\/repository\/\(.*\)\/\([^\/]*\)\/\([^\/]*\)\/\([^\/]*\)\.\([^\/\.]*\)\.lastUpdated$/\4/" )
    art_type=$( echo ${lu} | sed -e "s/^\/home\/fred\/\.m2\/repository\/\(.*\)\/\([^\/]*\)\/\([^\/]*\)\/\([^\/]*\)\.\([^\/\.]*\)\.lastUpdated$/\5/" )
    echo "artefact $grp_id : $art_id : $art_type : $art_vers"

    art_fic=${M2_REPO}/${grp_id}/${art_id}/${art_vers}/${art_id}-${art_vers}.${art_type}

    if [[ -e ${art_fic} ]]; then
        continue
    fi

    ${wget} --quiet \
        ${REPO_URL}/${grp_id}/${art_id}/${art_vers}/${art_id}-${art_vers}.${art_type} \
        --output-document=${art_fic}

    rm -f ${lu}
done

entrees=$( \ls ${dir} )
for e in ${entrees}; do
    fqdn=${dir}/${e}
    if [[ -d ${fqdn} ]]; then
        ${pgm} ${fqdn}
    fi
done
